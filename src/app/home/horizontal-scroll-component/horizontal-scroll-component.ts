import { Component, Input } from '@angular/core';
import { RestaurantItem, RestaurantService } from '@src/app/shared/restaurant.service';
import { PriceTagPipe } from '@src/app/shared/price-tag-pipe';
import { PlatformService } from '@src/app/shared/platform.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'horizontal-scroll-component',
    templateUrl: 'horizontal-scroll-component.html'
})
export class HorizontalScrollComponent {
    @Input() header: any;
    @Input() restaurantList: Array<RestaurantItem> = [];
    @Input() detailMode = false;
    @Input() restaurant: RestaurantItem;
    detailPhotoHeight: number;
    detailPhotoWidth: number;
    photoHeightRate = 0.09;
    photoWidthRate = 0.3;

    constructor(
        public priceTagPipe: PriceTagPipe,
        private restaurantService: RestaurantService,
        private platformService: PlatformService) {
            this.detailPhotoHeight = this.platformService.screenHeightPx * this.photoHeightRate;
            this.detailPhotoWidth = this.platformService.screenWidthPx * this.photoWidthRate;
        }

    goToRestaurant(detail: RestaurantItem) {
        this.restaurantService.setRestaurantDetail(detail);
    }
}
