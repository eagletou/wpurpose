import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import * as geolocation from 'nativescript-geolocation';
import { GeoLocationService } from './geo-location.service';

const firebase = require('nativescript-plugin-firebase/app');

export interface RestaurantItem {
    id: string;
    name: string;
    address1: string;
    address2: string;
    city: string;
    states: string;
    price_tage: string;
    slogen: string;
    type_tag: number;
    zip_code: string;
    image_url: Array<string>;
    geo_point: geolocation.Location;
    // distance unit is miles
    distance_from_user: number;
    category: Array<string>;
    restaurant_green_tag: string;
}

@Injectable({
    providedIn: 'root'
})
export class RestaurantService {

    meterToMileConvertRatio = 0.000621371;
    restaurantsCollection;
    menusCollection;
    storageRef;
    restaurantDetailOberser: BehaviorSubject<RestaurantItem> = new BehaviorSubject(null);
    restaurantsCache: BehaviorSubject<Array<RestaurantItem>> = new BehaviorSubject([]);

    constructor(private geoLocationService: GeoLocationService) {
        this.restaurantsCollection = firebase.firestore().collection('restaurants');
        this.storageRef = firebase.storage().ref();
        this.getAllRestaurants();
    }

    getAllRestaurants() {
        // This will cost a lots of money: const query = this.restaurantsCollection.where('search_index', 'array-contains', searchKeyWord);
        Promise.all(
            [this.restaurantsCollection.get(), this.geoLocationService.getCurrentLocation()]
        ).then(([value1, value2]) => {
            const restaurants = [];
            value1.forEach( i => {
                const data = i.data();
                data.id = i.id;
                const distance = this.geoLocationService.getDistance(data.geo_point) * this.meterToMileConvertRatio;
                data.distance_from_user = distance.toFixed(2);
                restaurants.push(data);
                });
                this.restaurantsCache.next(restaurants);
            });
    }

    setRestaurantDetail(detail: RestaurantItem) {
        this.restaurantDetailOberser.next(detail);
    }

    // getImage(id: string) {
    //     const childRef = this.storageRef.child(id + '.JPG');
    //     return new Promise<string>((resolve, reject) => {
    //         childRef.getDownloadURL().then(url => {
    //             resolve(url);
    //         }).catch(err => {
    //             reject(err);
    //         });
    //     });
    // }

    filterRestaurants(searchKeyWord: string) {
        let result = this.restaurantsCache.value;
        result = result.filter( item => {
            return item.name.includes(searchKeyWord);
        });
        return result;
    }

    getRestaurantMenu(restaurantId: string) {
        this.menusCollection = firebase.firestore().collection('menus');
        const query = this.menusCollection.where('restaurant_id', '==', restaurantId).get().then(data => {
            data.forEach( i => {
                console.log(i.data());
            });
        });
        return query;
    }

}
