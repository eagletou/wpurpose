import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { registerElement } from 'nativescript-angular/element-registry';
import { MapView, Marker, Position } from 'nativescript-google-maps-sdk';

// Important - must register MapView plugin in order to use in Angular templates
registerElement('MapView', () => MapView);

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.css'],
})
export class MapComponent implements OnInit {
    // Manhathan
    latitude =  40.758896;
    longitude = -73.985130;
    zoom = 13;
    minZoom = 0;
    maxZoom = 22;
    bearing = 0;
    mapView: MapView;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Use the 'ngOnInit' handler to initialize data for the view.
    }

    // Map events
    onMapReady(event) {
        console.log('Map Ready');

        this.mapView = event.object;

        console.log('Setting a marker...');

        const marker = new Marker();
        marker.position = Position.positionFromLatLng(40.758896, -73.985130);
        marker.title = 'Oister 57';
        marker.snippet = 'w/purpose tour';
        marker.userData = {index: 1};
        this.mapView.addMarker(marker);
        marker.showInfoWindow();
    }

}
