import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'priceTagPipe'
})
export class PriceTagPipe implements PipeTransform {
    transform(value: number): any {
        let priceTag = '';
        for (let i = 0; i < value; i++) {
            priceTag += '$';
        }
        return priceTag;
    }
}
