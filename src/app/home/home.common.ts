import { Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { RestaurantDetailComponent } from './restaurant-detail/restaurant-detail.component';
import { RestaurantMenuComponent } from './restaurant-menu/restaurant-menu.component';
import { PriceTagPipe } from '../shared/price-tag-pipe';
import { HorizontalScrollComponent } from './horizontal-scroll-component/horizontal-scroll-component';

export const routes: Routes = [
    { path: 'default', component: HomeComponent },
    { path: 'restaurantDetail', component: RestaurantDetailComponent },
    { path: 'restaurantMenu', component: RestaurantMenuComponent }
];

export const componentDeclarations = [
    HomeComponent,
    RestaurantDetailComponent,
    RestaurantMenuComponent,
    PriceTagPipe,
    HorizontalScrollComponent
];
