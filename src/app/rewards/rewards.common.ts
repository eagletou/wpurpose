import { RewardsComponent } from './rewards.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
    { path: 'default', component: RewardsComponent }
];

export const componentDeclarations: any[] = [
    RewardsComponent
];
