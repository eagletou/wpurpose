import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { RestaurantService, RestaurantItem } from '@src/app/shared/restaurant.service';
import { Page } from 'tns-core-modules/ui/page/page';

@Component({
    selector: 'app-restaurant-detail',
    templateUrl: './restaurant-detail.component.html'
})
export class RestaurantDetailComponent implements OnInit {
    restaurant: RestaurantItem;
    url: string;

    constructor(
        private restaurantService: RestaurantService,
        private location: Location,
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.restaurant = this.restaurantService.restaurantDetailOberser.getValue();
    }

    onBackTap(): void {
        this.location.back();
    }
}
