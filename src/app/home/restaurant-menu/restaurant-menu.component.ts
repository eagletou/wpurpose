import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';
import { Location } from '@angular/common';
import { RestaurantService, RestaurantItem } from '@src/app/shared/restaurant.service';

@Component({
    selector: 'app-restaurant-menu',
    templateUrl: './restaurant-menu.component.html'
})
export class RestaurantMenuComponent implements OnInit {
    restaurantDetail: RestaurantItem;

    constructor(
        private location: Location,
        private page: Page,
        private restaurantService: RestaurantService
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.restaurantDetail = this.restaurantService.restaurantDetailOberser.getValue();
        console.log(this.restaurantDetail.id);
        const menus = this.restaurantService.getRestaurantMenu(this.restaurantDetail.id);
        console.log(menus.restaurant_id);
    }



    onBackTap(): void {
        this.location.back();
    }
}
