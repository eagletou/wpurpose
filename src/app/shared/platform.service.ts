import { Injectable } from '@angular/core';

const platformModule = require('tns-core-modules/platform');


@Injectable({
    providedIn: 'root'
})
export class PlatformService {
    screenHeightPx: any;
    screenWidthPx: any;
    device: string;

    constructor() {
        this.screenHeightPx = platformModule.screen.mainScreen.heightPixels;
        this.screenWidthPx = platformModule.screen.mainScreen.widthPixels;
        if (platformModule.isIOS) {
            this.device = 'ios';
        } else {
            this.device = 'android';
        }
    }
}
