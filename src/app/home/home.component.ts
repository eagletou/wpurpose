import { Component, OnInit, ViewChild } from '@angular/core';
import { RestaurantItem, RestaurantService } from '../shared/restaurant.service';
import { EventData } from 'tns-core-modules/data/observable/observable';
import { TextField } from 'tns-core-modules/ui/text-field';
import { GeoLocationService } from '../shared/geo-location.service';
import { Page } from 'tns-core-modules/ui/page/page';

@Component({
    moduleId: module.id,
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
    restaurantsCollection;
    searchKeyWord = '';
    isSearch: boolean;
    searchResultFound: boolean;
    restaurants: Array<RestaurantItem> = [];
    plantBasedRestaurants: Array<RestaurantItem> = [];
    sustainableSeafoodRestaurants: Array<RestaurantItem> = [];
    zeroWasteRestaurants: Array<RestaurantItem> = [];

    constructor(
        private restaurantService: RestaurantService,
        private geoLocationService: GeoLocationService,
        private page: Page) {
        this.geoLocationService.getCurrentLocation();
        this.isSearch = false;
        this.searchResultFound = false;
        // This is for removing the action bar in Anfroid
        this.page.actionBarHidden = true;
    }

    ngOnInit(): void {
        this.restaurantService.restaurantsCache.subscribe( data => {
            if (data.length > 0) {
                data.forEach(restaurant => {
                if (restaurant.restaurant_green_tag === 'plant_based') {
                    this.plantBasedRestaurants.push(restaurant);
                } else if (restaurant.restaurant_green_tag === 'sustainable_seafood') {
                    this.sustainableSeafoodRestaurants.push(restaurant);
                } else {
                    this.zeroWasteRestaurants.push(restaurant);
                }
                });
            }
        });
    }

    goToRestaurant(detail: RestaurantItem) {
        this.searchKeyWord = detail.name;
        this.restaurantService.setRestaurantDetail(detail);
    }

    startSearch() {
        this.isSearch = true;
    }

    cancelSearch() {
        this.isSearch = false;
    }

    searchRestaurant(args: EventData) {
        const textField = <TextField>args.object;
        if (textField.text.length > 1) {
            this.restaurants = this.restaurantService.filterRestaurants(textField.text);
            if (this.restaurants.length > 0) {
                this.searchResultFound = true;
            } else {
                this.searchResultFound = false;
            }
        }
    }
}
