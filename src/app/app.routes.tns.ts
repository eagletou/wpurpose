import { NgModule } from '@angular/core';
import { NativeScriptRouterModule, NSEmptyOutletComponent } from 'nativescript-angular/router';
import { Routes } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      redirectTo: '/(homeTab:home/default//mapTab:map/default//rewardsTab:rewards/default//ordersTab:orders/default)',
      pathMatch: 'full'
  },
  {
      path: 'home',
      component: NSEmptyOutletComponent,
      loadChildren: () => import('../app/home/home.module').then((m) => m.HomeModule),
      outlet: 'homeTab'
  },
  {
      path: 'map',
      component: NSEmptyOutletComponent,
      loadChildren: () => import('../app/map/map.module').then((m) => m.MapModule),
      outlet: 'mapTab'
  },
  {
      path: 'orders',
      component: NSEmptyOutletComponent,
      loadChildren: () => import('../app/orders/orders.module').then((m) => m.OrdersModule),
      outlet: 'ordersTab'
  },
  {
      path: 'rewards',
      component: NSEmptyOutletComponent,
      loadChildren: () => import('../app/rewards/rewards.module').then((m) => m.RewardsModule),
      outlet: 'rewardsTab'
  },
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
