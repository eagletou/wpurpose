import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { PriceTagPipe } from '../shared/price-tag-pipe';
import { PlatformService } from '../shared/platform.service';

import {
    componentDeclarations,
    routes,
  } from './home.common';

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(routes),
        NativeScriptFormsModule
    ],
    exports: [
        NativeScriptRouterModule,
        PriceTagPipe
    ],
    declarations: [
      ...componentDeclarations
    ],
    providers: [
         PriceTagPipe
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeModule { }
