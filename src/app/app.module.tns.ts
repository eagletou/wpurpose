import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { AppRoutingModule } from '@src/app/app.routes.tns';
import { AppComponent } from '@src/app/app.component';

import { HomeModule } from './home/home.module';
import { MapModule } from './map/map.module';
import { OrdersModule } from './orders/orders.module';
import { RewardsModule } from './rewards/rewards.module';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
const platformModule = require('tns-core-modules/platform');
declare var GMSServices: any;
if (platformModule.isIOS) {
  GMSServices.provideAPIKey('AIzaSyBydmf_wTgt_omgMhhC2FLyCavFp1RpfAw');
}
// import { SearchViewModule } from 'nativescript-search-view/angular';

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    HomeModule,
    MapModule,
    OrdersModule,
    RewardsModule,
    NativeScriptFormsModule
    // SearchViewModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
