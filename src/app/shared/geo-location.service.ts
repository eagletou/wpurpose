import { Injectable } from '@angular/core';
import * as geolocation from 'nativescript-geolocation';
import { Accuracy } from 'tns-core-modules/ui/enums'; // used to describe at what accuracy the location should be get

@Injectable({
    providedIn: 'root'
})
export class GeoLocationService {
    currentLocation: geolocation.Location;

    constructor() {
        // this.getCurrentLocation();
    }

    getCurrentLocation() {
        geolocation.enableLocationRequest();
        // Get current location with high accuracy
        return new Promise((resolve) => {
            geolocation.getCurrentLocation({ desiredAccuracy: Accuracy.high, maximumAge: 5000, timeout: 20000 }).then( location => {
                this.currentLocation = location;
                resolve();
            }
            );
        });
    }

    getDistance(targetLocation: geolocation.Location) {
        return geolocation.distance(this.currentLocation, targetLocation);
    }
}
