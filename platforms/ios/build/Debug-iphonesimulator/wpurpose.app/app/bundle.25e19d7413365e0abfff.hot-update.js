webpackHotUpdate("bundle",{

/***/ "./app/map/map.component.html":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ActionBar class=\"withpurpose_style\">\n    <Label text=\"w/\"></Label>\n</ActionBar>\n\n<GridLayout class=\"page__content\">\n    <Label class=\"page__content-icon far\" text=\"&#xf279;\"></Label>\n    <Label class=\"page__content-placeholder\" text=\"Map page content goes here\"></Label>\n</GridLayout>\n");

/***/ }),

/***/ "./app/orders/orders.component.html":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ActionBar class=\"withpurpose_style\">\n    <Label text=\"w/\"></Label>\n</ActionBar>\n\n<GridLayout class=\"page__content\">\n    <Label class=\"page__content-icon fas\" text=\"&#xf1da;\"></Label>\n    <Label class=\"page__content-placeholder\" text=\"Orders page content goes here\"></Label>\n</GridLayout>\n");

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvbWFwL21hcC5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9hcHAvb3JkZXJzL29yZGVycy5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFlLDJQQUE0TCwwSDs7Ozs7Ozs7QUNBM007QUFBZSwyUEFBNEwsNkgiLCJmaWxlIjoiYnVuZGxlLjI1ZTE5ZDc0MTMzNjVlMGFiZmZmLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBcIjxBY3Rpb25CYXIgY2xhc3M9XFxcIndpdGhwdXJwb3NlX3N0eWxlXFxcIj5cXG4gICAgPExhYmVsIHRleHQ9XFxcIncvXFxcIj48L0xhYmVsPlxcbjwvQWN0aW9uQmFyPlxcblxcbjxHcmlkTGF5b3V0IGNsYXNzPVxcXCJwYWdlX19jb250ZW50XFxcIj5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlX19jb250ZW50LWljb24gZmFyXFxcIiB0ZXh0PVxcXCImI3hmMjc5O1xcXCI+PC9MYWJlbD5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlX19jb250ZW50LXBsYWNlaG9sZGVyXFxcIiB0ZXh0PVxcXCJNYXAgcGFnZSBjb250ZW50IGdvZXMgaGVyZVxcXCI+PC9MYWJlbD5cXG48L0dyaWRMYXlvdXQ+XFxuXCIiLCJleHBvcnQgZGVmYXVsdCBcIjxBY3Rpb25CYXIgY2xhc3M9XFxcIndpdGhwdXJwb3NlX3N0eWxlXFxcIj5cXG4gICAgPExhYmVsIHRleHQ9XFxcIncvXFxcIj48L0xhYmVsPlxcbjwvQWN0aW9uQmFyPlxcblxcbjxHcmlkTGF5b3V0IGNsYXNzPVxcXCJwYWdlX19jb250ZW50XFxcIj5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlX19jb250ZW50LWljb24gZmFzXFxcIiB0ZXh0PVxcXCImI3hmMWRhO1xcXCI+PC9MYWJlbD5cXG4gICAgPExhYmVsIGNsYXNzPVxcXCJwYWdlX19jb250ZW50LXBsYWNlaG9sZGVyXFxcIiB0ZXh0PVxcXCJPcmRlcnMgcGFnZSBjb250ZW50IGdvZXMgaGVyZVxcXCI+PC9MYWJlbD5cXG48L0dyaWRMYXlvdXQ+XFxuXCIiXSwic291cmNlUm9vdCI6IiJ9