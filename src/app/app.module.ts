import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '@src/app/app.routes';
import { AppComponent } from '@src/app/app.component';

import { HomeModule } from '@src/app/home/home.module';
import { MapModule } from './map/map.module';
import { RewardsModule } from './rewards/rewards.module';
import { OrdersModule } from './orders/orders.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    MapModule,
    RewardsModule,
    OrdersModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
