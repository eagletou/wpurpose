import { Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';

export const routes: Routes = [
    { path: 'default', component: OrdersComponent }
];

export const componentDeclarations: any[] = [
    OrdersComponent
];
