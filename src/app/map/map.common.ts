import { Routes } from '@angular/router';
import { MapComponent } from './map.component';

export const routes: Routes = [
    { path: 'default', component: MapComponent }
];

export const componentDeclarations: any[] = [
    MapComponent
];
